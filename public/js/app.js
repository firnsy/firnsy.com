$(document).ready(function() {
  /* initialise highlight blocks */
  hljs.initHighlightingOnLoad();
});

angular.module('app-core', ['ngStorage'])
  .config(['$localStorageProvider', function ($localStorageProvider) {
    $localStorageProvider.setKeyPrefix('firnsy-');
  }])
  .factory('Settings', function($localStorage) {
    var service = {
      getValue: getValue,
      setValue: setValue,
    };

    activate();

    return service;

    ////////

    function activate() {
      var settings = $localStorage.settings;

      if (typeof settings !== 'object') {
        $localStorage.settings = {};
      }
    }

    function getValue(key, defaultValue) {
      var value = angular.copy(defaultValue);
      var settings = $localStorage.settings;

      if (settings.hasOwnProperty(key)) {
        value = settings[key];
      } else {
        setValue(key, defaultValue);
      }

      return value;
    }

    function setValue(key, value) {
      return $localStorage.settings[key] = value;
    }
  })
  .factory('Theme', function($localStorage, Settings) {
    var THEMES = ['dark', 'light'];

    var _theme = '';

    var service = {
      getTheme: getTheme,
      listThemes: listThemes,
      setTheme: setTheme,
      toggleTheme: toggleTheme,
    };

    activate();

    return service;

    ////////

    function activate() {
      setTheme(Settings.getValue('theme', 'dark'));
    }

    function getTheme() {
      return _theme;
    }

    function listThemes() {
      return THEMES;
    }

    function setTheme(theme) {
      if (theme !== _theme && THEMES.indexOf(theme) !== -1) {
        Settings.setValue('theme', theme);
        _theme = theme;

        var removeClasses = THEMES.map(function(t) { return theme === t ? '' : 'theme-' + t; }).join('');

        $('body')
          .removeClass(removeClasses)
          .addClass('theme-' + theme);
      }
    }

    function toggleTheme() {
      setTheme(THEMES[(THEMES.indexOf(_theme) + 1) % THEMES.length]);
    }
  })
  .directive('fEnter', function() {
    return {
      restrict: 'A',
      link: function(scope, el, attrs) {
        el.bind("keydown keypress", function(e) {
          if (e.which === 13) {
            scope.$apply(function () {
              scope.$eval(attrs.fEnter);
            });
            e.preventDefault();
          }
        });
      }
    };
  })
  .directive('fTime', function() {
    return {
      restrict: 'E',
      scope: { epoch: '@', format: '@?' },
      template: '<span>{{time()}}</span>',
      link: function(scope, el, attrs) {
        scope.time = function() {
          var m = moment.unix(scope.epoch);
          var f = scope.format || 'YYYY-MM-DD HH:mm:ss (Z)';
          return m.format(f);
        }
      }
    };
  })
  .directive("fTooltip", function($timeout) {
    return {
      restrict: 'A',
      link: function(scope, el, attrs) {
        $timeout(function() {
          $(el)
            .data('toggle', 'tooltip')
            .data('html', true)
            .data('title', attrs.fTooltip)
            .tooltip();
        });
      }
    }
  });

angular.module('app', ['app-core'])
  .controller('AppController', function($timeout, $window, Settings, Theme) {
    LOGO_URLS = [
      '/rocks-the-kazbar',
      '/come/get/some',
      '/turn/it/up?to=11',
      '/always-choose-paper',
      '/?mode=awesome'
    ];

    var vm = angular.extend(this, {
      logo: {
        text: 'firnsy.com',
        index: 10
      },
    });

    angular.extend(this, {
      canAnimateLogo: canAnimateLogo,
      formatLogo: formatLogo,
      portal: portal,
      toggleTheme: toggleTheme,
    });

    activate();

    ////////

    function activate() {
      _animate();
    }

    function canAnimateLogo() {
      return $window.window.innerWidth > 770;
    }

    function formatLogo() {
      return vm.logo.text.substring(0, vm.logo.index);
    }

    function portal(e) {
      // how good is Sandra?!
      if (e.altKey && e.ctrlKey && e.shiftKey) {
        $window.location.href = '/portal';
      } else if (e.altKey) {
        console.log('foo');
        html2canvas(document.body).then(function(canvas) {
          canvas.classList.add('glitch-canvas');
          document.body.appendChild(canvas);
        });
      }
    }

    function toggleTheme() {
      Theme.toggleTheme();
    }

    /*
    ** PRIVATE
    */

    function _animate() {
      vm.logo.index = 10;
      vm.logo.text = 'firnsy.com' + LOGO_URLS[Math.floor(Math.random() * LOGO_URLS.length)];

      $timeout(_type, 15000 + Math.random() * 15000);
    }

    function _type() {
      vm.logo.index++;

      if (vm.logo.index <= vm.logo.text.length) {
        $timeout(_type, 75 + Math.random() * 100);
      } else {
        $timeout(_animate, 3000);
      }
    }
  });

angular.module('portal', ['app-core'])
  .controller('PortalController', function($http, $timeout, $window, Settings) {
    var vm = angular.extend(this, {
      state: {
        jumping: false
      },
      jumpTo: '',
      landing: {},
    });

    angular.extend(this, {
      canJump: canJump,
      clearLanding: clearLanding,
      formatLandingHeader: formatLandingHeader,
      formatLandingBody: formatLandingBody,
      hasLanding: hasLanding,
      isJumping: isJumping,
      jump: jump,
    });

    activate();

    ////////

    function activate() {
    }

    function canJump() {
      return !vm.state.jumping;
    }

    function clearLanding() {
      vm.landing = {};
    }

    function formatLandingHeader() {
      var a = Object.keys(vm.landing)[0];
      var b = Object.keys(vm.landing[a])[0];

      return b;
    }

    function formatLandingBody() {
      var a = Object.keys(vm.landing)[0];
      var b = Object.keys(vm.landing[a])[0];
      var c = Object.keys(vm.landing[a][b])[0];

      return c + " == " + vm.landing[a][b][c];
    }

    function hasLanding() {
      return !angular.equals(vm.landing, {});
    }

    function isJumping() {
      return vm.state.jumping;
    }

    function jump() {
      if (!!vm.jumpTo.trim().length) {
        vm.state.jumping = true;

        console.log('Calibrating jump to: ' + vm.jumpTo);

        $http.post('/api/portal', {q: vm.jumpTo})
          .then(function(res) {
            console.debug(res);
            vm.landing = res.data;
          }, function(err) {
            console.error(err);
            vm.landing = {};
          })
          .finally(function() {
            vm.jumpTo = '';
            vm.state.jumping = false; 
          });
      }
    }
  });
