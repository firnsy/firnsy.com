---
author: firnsy
category: posts
date: 2016-05-14
title: The Unofficial LCA hardware badge
tags: badge, lca, lca17, esp8266
---
A few weeks ago during an office visit, a good mate and colleague of mine was attending the B-Sides Canberra (BSC) information security conference and was showing of a hardware badge that they had commissioned for attendees. It was awesome! To the best of my knowledge, it was also the first hardware badge I had seen for an Australian "tech" conference. Did I say it was awesome?! It really was, but ...

<!--more-->

I first started appreciating hardware badges when Joe "Kingpin" Grand started designing DEFCON's hardware badges around 2006. He went on to design a total of 6 fantastic hardware badges each having a unique flavour up until Ryan "1o57" Clarke took over the reigns and took the DEFCON badge in a more creative direction which I think resonates the DEFCON ethos perfectly. Electronic badges, at that point in time, had become common place at every security conference in Europe and the US that it had become pass&#xe9;.
<div class="row"><div class="col-sm-6 offset-sm-3"><img class="img-border" src="/img/blog/dc-15-17-badges.jpg"></img></div></div>

So I'm in the office and my mate was showing off the badge, inbetween a few pokes that my scheduled arrival was preventing him from attending the conference on that day. So, we're all being impressed by the badge demo, and if you've spent anytime around a group of engineers you won't be surprised that it soon turned into a session of "Does it have X?", "What about Y?", and "Oh! if only it had Z!". Looking at the badge I was immediately reminded of the DEFCON badges and how I always wanted to snag one of my own. More over I was thinking how much I would love to have crack at designing one myself, if only to refresh some dormant skills.

At this point I've identified that the only way to get a hardware badge is to build one of my own, but it's hardly worth being a badge if it's not a "conference" badge. Now for some reason or another I don't get to many conferences, but when opportunity arises the [Linux Conf Australia](https://linux.conf.au) (LCA) is my first pick. If you haven't heard of it, LCA is the first event of the year on the schedule of international Linux / FOSS conference circuit. If you haven't been to one, get to one, and if you do hunt me down and say "G'day". [Hobart, Tasmania](https://hobart.lca2017.org/) will be hosting the conference for 2017. Since Tasmania has been on the TODO list for quite a while I'll be getting a 2-for-1.

Where was I? That's right, this post is about badges &dash; the hardware variety. LCA doesn't do a hardware badge, there's probably a million good reasons why not. However, I'm sure none of them could trump my desire to create an epic addition to the week of geekery that LCA already is. So after all that preamble, LCA is getting a limited #unofficial hardware badge.

Queue the design brief: "be at least twice as good as the 2016 BSC badge". If you have to ask "why?" I don't think you'll really understand.  With the design brief in hand, and a quick 5 minutes of brainstorming the following wishlist was settled upon:
 - 2x ESP8266-12E (with usb access),
 - IR Comms,
 - some buttons,
 - LCD 320x240
 - SD card
 - some RGB LEDs

As I haven't designed a board in ernest since I left University this provides a perfect opportunity to refresh those skills. Given that I'm targetting LCA my intention is to utilise as much open source software as possible, starting with KiCad EDA suite. Oh and shout out to my local hackerspace, I'll be prototyping all the hardware down at the [Ballarat Hackerspace](https://ballarathackerspace.org.au).

My current plan is to produce a limited run (approximately 50 boards) with a hard ceiling cost of $55. Any profit (which I'm not anticipating) will either be donated back to LCA or go forward to the next badge adventure. Taking a page out of [Dave Tulloh's](https://david.tulloh.id.au/) book and his [Linux powered microwave updates](https://david.tulloh.id.au/category/microwave/), I'll do the same here as progress continues. Hopefully more frequent than my annual blogging cadence <i class="fa fa-smile-o" style="color: #fd0;"></i>

I'll have a git repo up shortly on Git(Hub|Lab) so others can play along and contribute if they wish.
