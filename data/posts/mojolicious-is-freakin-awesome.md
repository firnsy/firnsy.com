---
author: firnsy
category: posts
date: 2014-10-31
title: Mojolicious is freakin' awesome!
tags: mojolicious, web, perl
---
When it comes to social media, I am definitely trailing the trend. After all I've just built this blog over the past few evenings. A blog you say? How very 2000's of me! Yes, yes, I know. It may not be as hip as it once was, but I continue to find a lot of useful tips, tricks and hints in the blogosphere. Just as often as I have over the past decade and feel the medium is a perfect candidate as a long term memory store.

<!--more-->

The existence of this blog is largely for selfish reasons, primarily I want to consolidate and archive the pieces of information that don't get exercised enough in my mind. Those tiny morsels of knowledge sufficiently exotic that you can't grok in the short time you need to apply it. Copy, paste, Enter, finally it works, forgotten! Should I find myself returning here on occasion to find an answer and actually find it then I would consider this entire endeavour a **huge** success. More so if others found it useful.

Time will tell.

So here we are. What are my plans from here? Well, to kick this journey off, I'm just going to give a shout out to what I consider the most awesome web microframework ever ... [Mojolicious]. Perl for me always tended to be a "get it done quick" type of language and not one I would ever consider for Enterprise or general production environment. Not specifically due to any constraints of the language but rather the shortage of in house expertise that would eventually be responsible for maintaining it. much I implemented a few concepts bwith POE and AnyEvent leveraging their event-driven frameworks. However, the amount of fun that Mojolicious puts into programming was too alluring to keep away.

Here's some code snippets that show off it's power and elegance ...

```perl
use Mojolicious::Lite;

get '/' => {text => 'I ♥ Mojolicious!'};

app->start;
```

That's right unicode support out of the box without even breaking a sweat.

Not impressed? Well how about some nifty route matching that you can access from your controllers as well as templates? Rapid prototyping of RESTful interfaces can happen in absolutely no time.

```perl
use Mojolicious::Lite;

get '/' => {text => 'I ♥ Mojolicious!'};
get '/:name' => sub {
  my $c = shift;
  my $n = $c->param('name');
  $c->stash(name => $n);
  $c->render('awesome');
};

app->start;

__DATA__
@@ awesome.html.ep
<html>
<head>
  <title>AWESOME-O 3000</title>
</head>
<body>
  <h1><%= $name %> is AWESOME!</h1>
</body>
</html>

```

All the examples in this post utilise the Lite form where everything is contained in a single file. There are tools built right in that allow you to grow your Lite app into a Full app improving maintainability as your code scales.

I'll show you one more snippet that broadly illustrates it's awesome power and flexibility.


```perl
use Mojolicious::Lite;

# Render template "index.html.ep" from the DATA section
get '/' => {template => 'index'};

# WebSocket service used by the template to extract the title from a web site
websocket '/title' => sub {
  my $c = shift;
  $c->on(message => sub {
    my ($c, $msg) = @_;
    my $title = $c->ua->get($msg)->res->dom->at('title')->text;
    $c->send($title);
  });
};

app->start;

__DATA__
@@ index.html.ep
% my $url = url_for 'title';
<script>
  var ws = new WebSocket('<%= $url->to_abs %>');
  ws.onmessage = function(e) { document.body.innerHTML += e.data };
  ws.onopen    = function(e) { ws.send('http://mojolicio.us') };
</script>
```

Could websockets be any easier? I remember having to hand roll my own Websocket components (when they first appeared) on top of POE and AnyEvent only a few years ago and I can honestly say that [Mojolicious] makes it a trivial undertaking. I've used it to power the entire backend over at the [Korora Project], large data analytics, and even provide enterprise monitoring for cyber security sensors.

And yes, in case you were wondering, this entire site is also powered by [Mojolicious]. MOJO ALL THE THINGS :)

On that note, I'd like to send a big thanks to sri and the entire #mojo community (on irc.perl.org) for their awesome work.

[Mojolicious]: http://mojolicio.us
[Korora Project]: https://kororaproject.org
