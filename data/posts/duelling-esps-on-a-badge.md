---
author: firnsy
category: posts
date: 2016-06-11
title: Duelling ESP8266s on a hardware badge
tags: badge, lca, lca17, esp8266
---
Look out, two blog posts in the same year, slow down "me". Speaking of slow though, it's been anything but slow the past few weeks. Fortunately I've been able dedicate quite a bit of idle time into the #badge and am thought it best provide an update of the progress.

<!--more-->

If you don't remember the design brief, let me recap, "be at least twice as good as the 2016 BSC badge". The initial brainstorming came up with some loose requirements as follows:
 - 2x ESP8266-12E (with usb access),
 - IR Comms,
 - some buttons,
 - LCD 320x240
 - SD card
 - some RGB LEDs

The good news is that those loose requirements are now firm with the only notable addition being a small vibrating motor, which I thought is much less prone to abuse at a conference of around 1000 participants.

<h3 class="text-center">Revision 2 Schematic and Layout</h3>
<div class="row">
  <div class="col-sm-6">
    <a href="https://gitlab.com/firnsy/lca-badge/raw/master/docs/images/lca-badge-r2-schematic.jpg" target="_blank"><img class="img-border" src="https://gitlab.com/firnsy/lca-badge/raw/master/docs/images/lca-badge-r2-schematic.jpg"></img></a>
  </div>
  <div class="col-sm-6">
    <a href="https://gitlab.com/firnsy/lca-badge/raw/master/docs/images/lca-badge-r2-overview.jpg" target="_blank"><img class="img-border" src="https://gitlab.com/firnsy/lca-badge/raw/master/docs/images/lca-badge-r2-overview.jpg"></img></a>
  </div>
</div>

I won't go over the schematic and layout in too much detail during this post as a few things have moved along in the past few days and I'm in the middle of the third revision which will be the first revision to get spun into a board. I'll leave a more detailed walk through of the shematic in a later post, but the the delta from R2 currently includes: moving the buttons away from the R-2R chain and onto an I2C I/O expander, the vibration motor will also be moved onto the I/O expander (and consequently onto the master ESP8266), the RGB chain and the IR Tx/Rx will move over to the secondary ESP8266.

In addition, the power component is completely untested and based purely on datasheet information so it's highly probable that an R4 will be required before the final spin. Overall I'm really happy with the progress so far.

I also mentioned that I would get a repo up as soon as possible, and now you can follow the progress at it's [new home on GitLab](https://gitlab.com/firnsy/lca-badge).
