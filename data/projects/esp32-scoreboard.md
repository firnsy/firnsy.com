---
author: firnsy
category: projects
date: 2018-12-01
title: ESP32 Scoreboard
tags: esp32
---
Woot

<!--more-->

Boot

<div class="gallery">
<div class="gallery-row">
<img src="https://i.imgur.com/MF5HyKd.jpg" />
<video controls><source type="video/mp4" src="https://i.imgur.com/BqsiM7O.mp4" /></video>
<img src="https://i.imgur.com/jx4aoTs.jpg" />
</div>
<div class="gallery-row">
<img src="https://i.imgur.com/X3Un7g9.jpg" />
<img src="https://i.imgur.com/gxRE8wU.jpg" />
<img src="https://i.imgur.com/QT0c6h2.jpg" />
</div>
<div class="gallery-row">
<video controls><source type="video/mp4" src="https://i.imgur.com/vQTbcNa.mp4" /></video>
<img src="https://i.imgur.com/waiDJGb.png" />
</div>
</div>
