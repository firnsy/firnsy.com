#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Jekyll;

use Mojo::Base 'Mojolicious::Plugin';

use CommonMark;
use File::Basename qw(basename);
use File::Spec;
use Mojo::File qw(path);
use Mojo::Util qw(decode dumper trim);
use Time::Piece;
use YAML::Tiny;

use constant TYPE_MAP => {
  html  => 'html',
  md    => 'markdown',
  txt   => 'text',
};

use constant DEBUG      => $ENV{MOJO_JEKYLL_DEBUG} || 0;
use constant POSTS_DIR   => './posts';
use constant POSTS_STYLE => 'markdown';

our $VERSION = '1.1';

has conf  => sub { +{} };
has files => sub { +[] };
has posts => sub { +[] };

sub register {
  my ($plugin, $app, $conf) = @_;

  # default values
  $conf->{path}  ||= POSTS_DIR;
  $conf->{style} ||= POSTS_STYLE;

  $plugin->conf($conf) if $conf;

  # prepare the cache
  $plugin->_cache_posts($conf->{path});

  $app->log->info(sprintf('Processing Jekyll posts at: %s (type: %s, files: %s, posts: %s)', $conf->{path}, $conf->{style}, scalar @{$plugin->files}, scalar @{$plugin->posts}));

  $app->helper('posts.summary' => sub {
    my $self = shift;
    my $params = @_%2 ? shift : {@_};

    my $posts = $plugin->posts;

    # filter on category
    if (!!$params->{category} && scalar @{$posts}) {
      $posts = [grep {$params->{category} eq $_->{category}} $posts->@*];
    }

    # parse any tags
    if (!!$params->{tags} && scalar @{$params->{tags}} && scalar @{$posts}) {
      $posts = [grep {_in_array(@{$params->{tags}}, $_->{tags})} $posts->@*];
    }

    # apply any limits
    if (!!$params->{limit}) {
      $posts = [splice($posts->@*, 0, $params->{limit})];
    }

    return $posts;
  });

  $app->helper('posts.get' => sub {
    my ($self, $slug) = @_;

    my ($post) = grep {$_->{slug} eq $slug} $plugin->posts->@*;

    return $post;
  });
}

sub _cache_posts {
  my $self = shift;
  my $path = shift;

  my $files = path($path)->list_tree->to_array;

  # sort most recent first
  my $posts = [sort {$b->{date} cmp $a->{date}} grep {defined} map {$self->_parse_file($_)} $files->@*];

  $self->files($files);
  $self->posts($posts);
}

sub _parse_file {
  my ($self, $path) = (shift, shift);

  # ensure the path exists
  warn 'Invalid path.' and return undef unless -r $path->to_string;

  # slurp
  my $bytes = decode 'UTF-8', $path->slurp;

  # ensure header exists
  warn 'No YAML header exists.' and return undef unless $bytes =~ m/---(.*)---\r?\n/s;

  my $data = Load($1);
  $bytes =~ s/---.*---\r?\n//s;

  warn 'No date specified.' and return undef unless $data->{date};

  # initiliase some members
  $data->{content} = '';
  $data->{excerpt} = '';
  $data->{style} = 'unknown';

  $data->{date_time} = Time::Piece->strptime($data->{date}, '%Y-%m-%d');

  # load filename implied details
  my $file = $path->basename;

  if ($file =~ m/^([^\.]+)\.(\w+)/) {
    $data->{slug} = $1;
    $data->{extension} = lc $2;
    $data->{style} = TYPE_MAP->{lc $2} // 'unknown';
  } else {
    $data->{slug} = $file;
  }

  # split tags
  $data->{tags} = [ map { trim $_ } split /,/, $data->{tags} ] if $data->{tags};

  # grab excerpt as appropriate
  $data->{excerpt} = _convert(trim($1), $data->{style}) if $bytes =~ m/(.*)<!--more-->\r?\n/s;

  # store content as appropriate
  $data->{content} = _convert(trim($bytes), $data->{style});

  return $data;
}

sub _convert {
  my ($c, $t) = (shift, shift // '');

  if ($t eq 'markdown') {
    $c = CommonMark->markdown_to_html($c);
  } elsif ($t eq 'text') {
    $c = "<pre>$c</pre>";
  }

  return $c;
}

# short circuit grep
sub _sgrep(&@) {
  my $t = shift @_;
  for (@_) { return 1 if &$t };
  return 0;
}

# check for element in array
sub _in_array {
  my $n = shift;
  my @h = ref $_[0] eq 'ARRAY' ? @{$_[0]} : @_;
  return scalar(_sgrep { $n eq $_ } @h);
}

1;
