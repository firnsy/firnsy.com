#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::GitLabHub;

use Mojo::Base 'Mojolicious::Plugin';

use Mojo::UserAgent;
use Mojo::Util 'dumper';
use Time::Piece;

my $contributions = [];


has 'user';

sub register {
  my ($plugin, $app, $conf) = @_;

  die unless !!$conf->{user};

  my $user = $conf->{user};
  my $update_interval = $conf->{update_interval} // 10 * 60; # default to 10 minutes

  $app->log->info("GitLabHub collecting on user: $user");

  $plugin->user($user);
  $plugin->_fetch_latest_contributions;

  Mojo::IOLoop->recurring($update_interval, sub {
    $app->log->info("Collecting latest contributions for: $user");
    $plugin->_fetch_latest_contributions;
  });

  $app->helper('gitlabhub.calendar' => sub {
    my $c = shift;

    my $months = [];
    my $active_month = Time::Piece->strptime($contributions->[0]->{date}, '%F')->month;

    for my $w (0..51) {
      my $m = Time::Piece->strptime($contributions->[$w * 7]->{date}, '%F')->month;

      if ($m ne $active_month) {
        $active_month = $m;
        $months->[$w] = $m;
      } else {
        $months->[$w] = '';
      }
    }

    return {
      contributions => $contributions,
      months        => $months,
      offset_woy    => Time::Piece->strptime($contributions->[0]->{date}, '%F')->week,
      offset_dow    => Time::Piece->strptime($contributions->[0]->{date}, '%F')->day_of_week,
    };
  });
}

sub _fetch_latest_contributions {
  my $self = shift;

  my $ua = Mojo::UserAgent->new;
  my $now = gmtime;

  # gitlab returns a nice json structure that has key/value for every
  # non-zero commit date in the past year
  my $latest = $ua->get('https://gitlab.com/users/' . $self->user . '/calendar.json')
    ->result
    ->json;

  # github returns a post-processed svg which we need to reverse
  $ua->get('https://github.com/users/' . $self->user . '/contributions')
    ->result
    ->dom
    ->find('.js-calendar-graph-svg > g > g > rect')
    ->each(sub {
        my $e = shift;

        my $date = $e->attr('data-date');
        my $count = $e->attr('data-count');

        # add github contriubtion counts to existing gitlab ones
        $latest->{$date} = ($latest->{$date} // 0) +  $count;
      });

  $contributions = [map { +{date => $_, count => $latest->{$_}} } sort { $a cmp $b } keys %{$latest}];
}

1;
