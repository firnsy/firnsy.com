#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Memcache;
use Mojo::Base 'Mojolicious::Plugin';

use Cache::Memcached;
use Mojo::Util 'dumper';

our $VERSION = '0.1';

has 'memcache' => undef;
has 'prefix' => sub { return ''; };

sub register {
  my ($self, $app, $config) = @_;

  my $mem = new Cache::Memcached {
    servers => ['localhost:11211'],
    debug   => 0,
    compress_threshold => 10000
  };

  my $stats = $mem->stats;
  $mem->flush_all;

  $self->memcache($mem);

  $app->helper('cache.get' => sub {
    my ($c, $k, $d) = (shift, shift);

    my $v = $self->memcache->get($self->prefix.$k);

    if (!defined($v) && defined($d)) {
      $c->memcache->set($self->prefix.$k, $d);
      $v = $d;
    }

    return $v;
  });

  $app->helper('cache.set' => sub {
    my ($c, $k, $v) = (shift, shift, shift);

    $self->memcache->set($self->prefix.$k, $v);
  });
}

1;

