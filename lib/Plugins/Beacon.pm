#
# Licensed to the Apache Software Foundation (ASF) under one or or more
# contributor license agreements. See the NOTICE file distributed with this
# work for additional information regarding copyright ownership. The ASF
# licenses this file to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#

package Plugins::Beacon;
use Mojo::Base 'Mojolicious::Plugin';

use Mojo::Util 'dumper';

our $VERSION = '0.1';

has 'ip' => sub {+{}};

sub register {
  my ($plugin, $app, $conf) = @_;

  die unless !!$conf->{key};

  $app->routes->post('/api/beacon')->to(cb => sub {
    my $c = shift;
    my $key = $c->param('key') // '';
    my $host = $c->param('host') // '';

    if ($key eq $conf->{key}) {
      my $beacon_list = $c->cache->get('beacon', {});

      $beacon_list->{$host} = $c->remote_ip;
      $c->cache->set('beacon', $beacon_list);

      $app->log->info("IP List: ", join(', ', keys $beacon_list->%*));
    }

    $c->render(text => 'ok');
  });

  $app->log->info("Beacon online.");

  $app->helper('beacon.has_host' => sub {
    my ($c, $host) = (shift, shift);
    my $beacon_list = $c->cache->get('beacon', {});

    return exists($beacon_list->{$host});
  });

  $app->helper('beacon.ip' => sub {
    my ($c, $host) = (shift, shift);
    my $beacon_list = $c->cache->get('beacon', {});

    return $beacon_list->{$host};
  });
}

1;
