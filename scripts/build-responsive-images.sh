#!/bin/bash

usage() {
  echo "Usage: $0 [-o path] -i path" 1>&2;
  exit 1;
}

while getopts ":i:o:" o;
do
  case "${o}" in
    o)
      OUTPUT_PATH="$(realpath ${OPTARG})"
      ;;
    i)
      INPUT_PATH="$(realpath ${OPTARG})"
      [ -z "${OUTPUT_PATH}" ] && OUTPUT_PATH=${INPUT_PATH}

      ;;
    *)
      usage
      ;;
  esac
done
shift $((OPTIND-1))

if [ -z "${INPUT_PATH}" ];
then
  usage
fi


echo "Parsing: ${INPUT_PATH} => ${OUTPUT_PATH}"

echo "Building directories ..."
for RES in "300" "600" "1000" "2000";
do
  [ -d "${OUTPUT_PATH}/w${RES}" ] || mkdir -p ${OUTPUT_PATH}/w${RES}
done

for FILE in ${INPUT_PATH}/*jpg;
do
  echo "Found: ${FILE}"

  FILE_BASE=$(basename ${FILE})

  for RES in "300" "600" "1000" "2000";
  do
    convert -resize ${RES}x ${FILE} ${OUTPUT_PATH}/w${RES}/${FILE_BASE}
  done
done
